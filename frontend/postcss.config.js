const { join } = require('path');

module.exports = {
  plugins: {
    "postcss-import": {},
    "tailwindcss": {config: join(__dirname, 'tailwind.config.js')},
    "postcss-flexbugs-fixes": {},
    "postcss-nesting": {},
    "postcss-custom-properties": {},
    "autoprefixer": {},
  },
}
