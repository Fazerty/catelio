import { getStrapiMedia } from "../lib/media";
import NextImage from "next/image";

const Image = ({ image }:{ image: any }) => {
  const { alternativeText, width, height } = image.data.attributes;
  console.log('---------------')
  console.log(image, width, height)

  return (
    <NextImage
      layout="responsive"
      width={width}
      height={height}
      objectFit="contain"
      src={getStrapiMedia(image) || "#"}
      alt={alternativeText || ""}
    />
  );
};

export default Image;