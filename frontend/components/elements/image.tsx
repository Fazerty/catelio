import { getStrapiMedia } from "../../lib/media"
import Image from "next/image"
import PropTypes from "prop-types"
import { mediaPropTypes } from "../../lib/types"

const NextImage = ({ media, ...props }: any) => {
  const { url, alternativeText, width, height } = media.data.attributes

  const loader = ({ src, width, quality }: any) => {
    return getStrapiMedia(src) || ""
  }

  // The image has a fixed width and height
  if (props.width && props.height) {
    return (
      <Image loader={loader} src={url} alt={alternativeText || ""} {...props}  /> // TODO: check why unoptimized is sometimes mandatory
    )
  }

  // The image is responsive
  return (
    <Image
      loader={loader}
      layout="responsive"
      width={width || "100%"}
      height={height || "100%"}
      objectFit="contain"
       // TODO: check why unoptimized is sometimes mandatory
      src={url}
      alt={alternativeText || ""}
    />
  )
}
/*
Image.propTypes = {
  media: mediaPropTypes.isRequired,
  className: PropTypes.string,
}
*/
export default NextImage
