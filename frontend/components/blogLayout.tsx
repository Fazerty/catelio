import { GlobalContext } from "../pages/_app";
import Footer from "./elements/footer";
import Nav from "./nav";

const BlogLayout = ({ children, categories }: { children: any, categories: any }) => (

  <GlobalContext.Consumer>
    {global => (
      <div className="flex flex-col justify-between min-h-screen">
        <>{/* Aligned to the top */}
        <div className="flex-1">
          <Nav categories={categories} />
          {children}
          <div>{children}</div>
        </div>

        {/* Aligned to the bottom */}
        </>
      </div>)
    
    }
  </GlobalContext.Consumer>
);

export default BlogLayout;