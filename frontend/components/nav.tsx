import React from "react";
import Link from "next/link";

const Nav = ({ categories }: { categories: any }) => {
  return (
    <div>
      <nav className="border-gray-200 border-b-2 py-6 sm:py-2">
        <div className="container flex flex-row items-center justify-between">

          <ul className="list-none md:flex flex-row gap-4 items-baseline ml-10">
            <li>
              <Link href="/">
                <a>Strapi Blog</a>
              </Link>
            </li>
          </ul>
        </div>
        <div className="container flex flex-row items-center justify-between">
          <ul className="list-none md:flex flex-row gap-4 items-baseline ml-10">
            {categories.map((category: { id: React.Key | null | undefined; attributes: { slug: any; name: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }; }) => {
              return (
                <li key={category.id}>
                  <Link href={`/blog/category/${category.attributes.slug}`}>
                    <a className="uk-link-reset">{category.attributes.name}</a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Nav;