import { fetchAPI } from "./api"

export async function getLocalizedPage(targetLocale: any, pageContext: { localizations: { data: any[] } }) {
  const localization = pageContext.localizations.data.find(
    (localization: { attributes: { locale: any } }) => localization.attributes.locale === targetLocale
  )
  const localePage = await fetchAPI(`/pages/${localization.id}`)
  return localePage
}

export function localizePath(page: { locale: any; defaultLocale: any; slug: any }) {
  const { locale, defaultLocale, slug } = page

  if (locale === defaultLocale) {
    // The default locale is not prefixed
    return `/${slug}`
  }

  // The slug should have a localePrefix
  return `/${locale}/${slug}`
}

export function getLocalizedPaths(page: { locale?: any; locales: any; defaultLocale?: any; slug: any; localizations?: any }) {
  const paths = page.locales.map((locale: any) => {
    return {
      locale: locale,
      href: localizePath({ ...page, locale, defaultLocale: page.defaultLocale || "en"}),
    }
  })

  return paths
}
