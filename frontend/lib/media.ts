import { getStrapiURL } from "./api";

export function getStrapiMedia(url?: string | null | any) {
  if (!url) {
    return undefined
  }

  if (url?.data) {
    url = url.data.attributes.url
  }
  // Return the full URL if the media is hosted on an external provider
  if (url.startsWith("http") || url.startsWith("//")) {
    return url
  }
  const imageUrl = url.startsWith("/") ? getStrapiURL(url) : url;
  return imageUrl;
}