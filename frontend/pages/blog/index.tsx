import { ApolloQueryResult, gql } from "@apollo/client";
import React from "react";
import Articles from "../../components/articles";
import Layout from "../../components/blogLayout";
import { initializeApollo } from "../../lib/apolloClient";
import { ArticleEntity, ArticleEntityResponseCollection, CategoryEntity, CategoryEntityResponseCollection, GlobalEntity, GlobalEntityResponse }
  from "../../models";

const Home = ({ articles, categories }: { articles: Array<ArticleEntity>, categories: Array<CategoryEntity>, homepage: GlobalEntity }) => {
  return (
    <Layout categories={categories}>
      <div className="flex">
        <div className="flex-1">
          <Articles articles={articles} />
        </div>
      </div>
    </Layout>
  );
};

const InfosQuery = gql`
  query InfosQuery {
    categories {
      data {
        attributes {
          name, 
          slug,
          description
          }
        }
      }
    articles {
      data {
        id
        attributes {
          slug
          category {
            data {
              attributes {
                name
                slug
                description
              }
            }
          }
          cover {
            data {
              attributes {
                name
                alternativeText
                url
                width
                height
              }
            }
          }
        }
      }
    }
  }
  
`

export async function getStaticProps() {

  const apolloClient = initializeApollo()

  const infoResponse: ApolloQueryResult<{ articles: ArticleEntityResponseCollection, categories: CategoryEntityResponseCollection, global: GlobalEntityResponse }> = await
    apolloClient.query({
      query: InfosQuery,
    })
    ;

  return {
    props: {
      articles: infoResponse.data.articles.data,
      categories: infoResponse.data.categories.data,
    },
    revalidate: 1,
  };
}

export default Home;
