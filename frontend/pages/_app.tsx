import '../styles/globals.css'
import type { AppContext } from 'next/app'
import App from "next/app";
import Head from "next/head";
import { createContext } from "react";
import { getStrapiMedia } from "../lib/media";
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from '@apollo/client';
import { getStrapiURL } from '../lib/api';
import ErrorPage from "next/error"
import { DefaultSeo } from "next-seo"
import { getGlobalData } from "../lib/api"


// Store Strapi Global object in context
export const GlobalContext = createContext<{ defaultSeo?: any, siteName?: string }>({});

const CatelioFrontApp = ({ Component, pageProps }: { Component: any, pageProps: any }) => {
  const { global } = pageProps;
  
  if (global == null) {
    return <ErrorPage statusCode={404} >{}</ErrorPage>
  }

  const { metadata, favicon, metaTitleSuffix } = (global.data || global).attributes; // TODO: investigate

  const httpLink = createHttpLink({
    uri: getStrapiURL('/graphql')
  });

  const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
  });

  return (
    <ApolloProvider client={client}>
      {/* Favicon */}
      <Head>
        <link
          rel="shortcut icon"
          href={getStrapiMedia(favicon.data.attributes.url) || "#"}
        />
      </Head>
      {/* Global site metadata */}
      <DefaultSeo
        titleTemplate={`%s | ${metaTitleSuffix}`}
        title="Page"
        description={metadata.metaDescription}
        openGraph={{
          images: Object.values(
            metadata.shareImage.data.attributes.formats
          ).map((image : any) => {
            return {
              url: getStrapiMedia(image.url) || "#",
              width: image.width,
              height: image.height,
            }
          }),
        }}
        twitter={{
          cardType: metadata.twitterCardType,
          handle: metadata.twitterUsername,
        }}
      />
      {/* Display the content */}
      <GlobalContext.Provider value={global.attributes}>
        <Component {...pageProps} />
      </GlobalContext.Provider>
    </ApolloProvider>
  );
};

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So article, category and home pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
CatelioFrontApp.getInitialProps = async (appContext: AppContext) => {
  // Calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  const globalLocale = await getGlobalData(appContext.router.locale || 'fr')
  // Pass the data to our page via props
  return {
    ...appProps,
    pageProps: {
      global: globalLocale,
    },
  }
};

export default CatelioFrontApp;
