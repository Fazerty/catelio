# Catelio

## Use latest version of Yarn:

    npm install --global yarn
    yarn set version berry


## First start to set the admin of Strapi backend

    yarn backend:dev


## To start

Only the backend:

    yarn backend:dev

Only the frontend:

    yarn frontend:dev

Both:

    yarn dev



## To generate frontend code from your GraphQL schema when modification are made in the backend

    yarn graphql:codegen