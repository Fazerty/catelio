const { global, pages } = require("./en");
const { globalFR, pagesFR } = require("./fr");
const { about, categories, authors, articles } = require("./blog.json");
const { leadFormSubmissions } = require("./lead-form-submissions.json");

module.exports = {
  globals: [global, globalFR],
  pages: [...pages, ...pagesFR],
  leadFormSubmissions,
  about, categories, authors, articles
};
